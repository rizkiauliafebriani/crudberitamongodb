const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
  no: {
    type: String
  },
  id_berita: {
    type: String
  },
  judul: {
    type: String
  },
  penulis: {
    type: String
  },
  editor: {
    type: String
  },
  waktu_akses: {
    type: Date
  },
  info_device: {
    type: String
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Tasks", TaskSchema);